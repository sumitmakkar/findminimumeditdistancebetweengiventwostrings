#include<iostream>
#include<vector>
#include<algorithm>

using namespace std;


class Engine
{
private:
    string str1;
    string str2;
    vector< vector<int> > matrixVector;
    
    void initializeMatrix()
    {
        int str1Len = (int)str1.length();
        int str2Len = (int)str2.length();
        for(int i = 0 ; i <= str1Len ; i++)
        {
            vector<int> innerVector;
            for(int j = 0 ; j <= str2Len ; j++)
            {
                if(!i)
                {
                    innerVector.push_back(j);
                }
                else if(!j)
                {
                    innerVector.push_back(i);
                }
                else
                {
                    innerVector.push_back(0);
                }
            }
            matrixVector.push_back(innerVector);
        }
    }
    
    void displayMatrix()
    {
        int str1Len = (int)str1.length();
        int str2Len = (int)str2.length();
        for(int i = 0 ; i <= str1Len ; i++)
        {
            for(int j = 0 ; j <= str2Len ; j++)
            {
                cout<<matrixVector[i][j]<<" ";
            }
            cout<<endl;
        }
    }
    
public:
    
    Engine(string s1 , string s2)
    {
        str1 = s1;
        str2 = s2;
        initializeMatrix();
    }
    
    int findMinimumEdits()
    {
        int str1Len = (int)str1.length();
        int str2Len = (int)str2.length();
        for(int i = 1 ; i <= str1Len ; i++)
        {
            for(int j = 1 ; j <= str2Len ; j++)
            {
                if(str1[i-1] == str2[j-1])
                {
                    matrixVector[i][j] = matrixVector[i-1][j-1];
                }
                else
                {
                    matrixVector[i][j] = min(matrixVector[i][j-1]   , matrixVector[i-1][j]);
                    matrixVector[i][j] = min(matrixVector[i-1][j-1] , matrixVector[i][j]);
                    matrixVector[i][j]++;
                }
            }
        }
        return matrixVector[str1Len][str2Len];
    }
};

int main()
{
    string str1 = "INTENTION";
    string str2 = "EXECUTION";
    //    string str1 = "KITTEN";
    //    string str2 = "SITTIG";
    Engine e    = Engine(str1 , str2);
    cout<<e.findMinimumEdits()<<endl;
    return 0;
}
